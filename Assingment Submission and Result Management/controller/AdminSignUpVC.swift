//
//  AdminSignUpVC.swift
//  Assingment Submission and Result Management
//
//  Created by Zahid Shaikh on 14/09/17.
//  Copyright © 2017 Zahid Shaikh. All rights reserved.
//

import UIKit

class AdminSignUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    let array = [101,102,103,104,105,106,107,108,109,110,111,112,113,114,115]
    let array1 = [1992,1993,1994,1995,1996,1997,1998,1999,2000]
    let array2 = ["June","December"]
    let array3 = [1,2,3,4,5,6]
    var activeField: UITextField?
    var tempArray: Array! = []
    var stype = DetailsType.centerCode

    var popupController:CNPPopupController?
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var studentNameText: UITextField!
    @IBOutlet weak var enrollmentText: UITextField!
    @IBOutlet weak var centerNameText: UITextField!
    @IBOutlet weak var centerCodeText: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var yearText: UITextField!
    @IBOutlet weak var sessionText: UITextField!
    @IBOutlet weak var semesterText: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var backGImageV: UIImageView!

    var studrecord:StudentRecord!
    var enrollmentStr = ""
    //MARK:- ViewlifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.studentNameText.delegate = self
        self.enrollmentText.delegate = self
        self.centerNameText.delegate = self
        self.centerCodeText.delegate = self
        self.yearText.delegate = self
        self.sessionText.delegate = self
        self.semesterText.delegate = self
        
        enrollmentText.text = enrollmentStr
        
        backGImageV.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AdminSignUpVC.dismissKeyBoard)))
        registerForKeyboardNotifications()
        studentNameText.becomeFirstResponder()
        
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        deregisterFromKeyboardNotifications()
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let assignVC: AssignSubmissionVC = segue.destination as! AssignSubmissionVC
//        assignVC.studentRecord = studrecord
//    }

    
    func textShouldBeginEditing(textView: UITextField) -> Bool {
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.barStyle = UIBarStyle.default
        keyboardToolBar.isTranslucent = true;
        let nextBtn = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target:self, action: #selector(AdminSignUpVC.resignKeyboard))
        let preBtn = UIBarButtonItem(title: "Previous", style: UIBarButtonItemStyle.done, target:self, action: #selector(AdminSignUpVC.resignKeyboard))
        keyboardToolBar.setItems([nextBtn,preBtn], animated: true)
        keyboardToolBar.sizeToFit()
        textView.inputAccessoryView = keyboardToolBar
        return true;
    }
    @objc func resignKeyboard(){
        self.view.endEditing(true)
        
    }
    
    //MARK:- TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tablecell") as! TableAlertVCell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "tablecell", for: indexPath)
        cell.nameLbl?.text = String(describing: tempArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if stype == .centerCode {
            centerCodeText.text = String(describing: tempArray[indexPath.row])
 
        }else if stype == .year {
            yearText.text = String(describing: tempArray[indexPath.row])
            
        }else if stype == .session {
            sessionText.text = String(describing: tempArray[indexPath.row])
        }else if stype == .semester {
            semesterText.text = String(describing: tempArray[indexPath.row])
        }
        
        clickOnDismissButton(UIButton())
    }
    //MARK:- Keyboard hiding methods
    
    @objc func  dismissKeyBoard() {
        activeField?.resignFirstResponder()
        adjustScrollViewContentSize()
        
    }

    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        //var info = notification.userInfo!
       // let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        adjustScrollViewContentSize()
    }
    
    func adjustScrollViewContentSize(){
        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(64.0, 0.0, 0 , 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollview.isScrollEnabled = true
        self.scrollview.setContentOffset(CGPoint(x:0, y:-64.0), animated: true)
        
    }
    
    //MARK:- Text Field Delegates Methods
    func textFieldDidBeginEditing(_ textField: UITextField){
        textShouldBeginEditing(textView: textField)
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField = textField
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true;
    }
    
    //MARK:- @IBAction Methods

    func saveStudentRecord() {
        studrecord =  StudentRecord()
        studrecord.name = (studentNameText?.text)!
        studrecord.enrollmentNo = Int((enrollmentText?.text)!)!
        studrecord.studycenterName = (centerNameText?.text)!
        studrecord.studycenterCode = (centerCodeText?.text)!
        studrecord.session = (sessionText?.text)!
        studrecord.year = Int((yearText?.text)!)!
        studrecord.semester = Int((semesterText?.text)!)!
        DatabaseHandler.sharedInstance.saveStudentRecord(studentRecord: studrecord)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil) as UIStoryboard
        let singleCVC = storyboard.instantiateViewController(withIdentifier: "AssignSubmissionVC") as! AssignSubmissionVC
        singleCVC.studentRecord = studrecord
        self.navigationController?.pushViewController(singleCVC, animated: true)

    }
    @IBAction func submitBtnPressed(_ sender: Any){

        if (studentNameText.text?.characters.count)! <= 0 || enrollmentText.text! == "" || centerNameText.text == "" || centerCodeText.text == "" || yearText.text == "" || sessionText.text == "" || semesterText.text == "" {
                        let alert = UIAlertController(title: "Alert", message: "No Field Can Be Empt", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Back", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
//            submitBtn.isEnabled = false
        } else {
            saveStudentRecord()
        }
        
        
    }

    @IBAction func btnPressed(_ sender: Any) {
        //showNewUserPoPup()
        tempArray = array
        stype = .centerCode
        showTableAlertPopup("Center Code")
        self.view.endEditing(true)
//        self.scrollview.isScrollEnabled = true
    }
    
    @IBAction func yearBtnPressed(_ sender: Any) {
        //showNewUserPoPup()
        tempArray = array1
        stype = .year
        showTableAlertPopup("Year")
        self.view.endEditing(true)
//        self.scrollview.isScrollEnabled = true
        
    }
    
    @IBAction func sessionBtnPressed(_ sender: Any) {
        //showNewUserPoPup()
        tempArray = array2
        stype = .session
        showTableAlertPopup("Session")
        self.view.endEditing(true)
//        self.scrollview.isScrollEnabled = true
    }
    @IBAction func semesterBtnPressed(_ sender: Any) {
        //showNewUserPoPup()
        tempArray = array3
        stype = .semester
        showTableAlertPopup("Semester")
        self.view.endEditing(true)
//        self.scrollview.isScrollEnabled = true
    }
    
    //MARK: - PopupAlert
    
    func showTableAlertPopup(_ titleName:String) {
        
        if self.popupController != nil {
            self.popupController = nil
        }
        
        let customeview = Bundle.main.loadNibNamed("TableViewAlertView", owner: self, options: nil)?[0] as! UIView
        
        let tableVheight = (self.view.frame.height) / 2//
        customeview.frame = CGRect(x: customeview.frame.origin.x, y: customeview.frame.origin.y, width: customeview.frame.size.width, height: CGFloat(tableVheight))
        
        
        let sf = self.view.frame;
        let f = customeview.frame
        
        let newHeight = f.size.height
        
        let customeV = UIView(frame: CGRect(x:  f.origin.x, y:  f.origin.y, width: sf.size.width - 34, height: newHeight + 6))
        customeV.tag = 90
        customeV.backgroundColor = UIColor.init(white: 0.0, alpha: 0.4)
        customeV.layer.cornerRadius = 4.0
        customeview.layer.cornerRadius = 4.0
        
        customeview.frame = CGRect(x: 3, y: 3, width: sf.size.width - 40, height: newHeight)
        customeV.addSubview(customeview)
        
        
        let titleLbl:UILabel = customeview.viewWithTag(1) as! UILabel
        titleLbl.text = titleName
        
        let tableV:UITableView = customeview.viewWithTag(2) as! UITableView
        tableView = tableV
        tableView?.delegate = self
        tableView?.dataSource = self
        
        let nib = UINib(nibName: "TableAlertCell", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: "tablecell")
        
        
        let cancelBtn:UIButton = customeview.viewWithTag(3) as! UIButton
        cancelBtn.addTarget(self, action:#selector(AdminSignUpVC.clickOnDismissButton(_:)), for: .touchUpInside)
        
        let popupController = CNPPopupController(contents:[customeV])
        popupController.theme = CNPPopupTheme.default()
        popupController.theme.popupStyle = CNPPopupStyle.centered
        popupController.heightAdjust = false
        self.popupController = popupController
        popupController.present(animated: true)
        
        tableView?.reloadData()
        
    }
    
    @IBAction func clickOnDismissButton(_ sender: UIButton) {
        self.popupController?.dismiss(animated: true)
    }

    func showNewUserPoPup() {
        
        if self.popupController != nil {
            self.popupController = nil
        }
        
        let customeview = Bundle.main.loadNibNamed("CenterCodeView", owner: self, options: nil)?[0] as! UIView
        let popupController = CNPPopupController(contents:[customeview])
        popupController.theme = CNPPopupTheme.default()
        popupController.theme.popupStyle = CNPPopupStyle.centered
        popupController.heightAdjust = false
        self.popupController = popupController
        popupController.present(animated: true)
        
    }

}
enum DetailsType :Int {
    case centerCode
    case year
    case session
    case semester
    
}
class TableAlertVCell: UITableViewCell {
    
    @IBOutlet var nameLbl: UILabel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
