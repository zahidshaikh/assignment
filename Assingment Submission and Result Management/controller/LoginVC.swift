//
//  LoginVC.swift
//  Assingment Submission and Result Management
//
//  Created by Zahid Shaikh on 14/09/17.
//  Copyright © 2017 Zahid Shaikh. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var enrolText: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //
    }

    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        
    }
    @IBAction func clickOnNextButton(_ sender: UIButton) {
        
        if (enrolText?.text?.isEmpty)!{
            return
        }
        
        
        let enrollNo:Int = Int((enrolText?.text)!)!
        
        if enrollNo > 0 {
            let strec:StudentRecord? = DatabaseHandler.sharedInstance.getRecordByEnrollmentNo(enrollNo)
            if strec != nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil) as UIStoryboard
                let singleCVC = storyboard.instantiateViewController(withIdentifier: "AssignSubmissionVC") as! AssignSubmissionVC
                singleCVC.studentRecord = strec
                self.navigationController?.pushViewController(singleCVC, animated: true)
                
            }else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil) as UIStoryboard
                let singleCVC = storyboard.instantiateViewController(withIdentifier: "AdminSignUpVC") as! AdminSignUpVC
                singleCVC.enrollmentStr = (enrolText?.text)!
                self.navigationController?.pushViewController(singleCVC, animated: true)
            }
        }

    }

}
