//
//  AssignSubmissionVC.swift
//  Assingment Submission and Result Management
//
//  Created by Zahid Shaikh on 20/09/17.
//  Copyright © 2017 Zahid Shaikh. All rights reserved.
//

import UIKit

class AssignSubmissionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var uncheckBtn: UIButton!
    @IBOutlet weak var tableVieww: UITableView!
    @IBOutlet weak var subjectCodeLbl: UILabel!
    @IBOutlet weak var semesterLbl: UILabel!
    @IBOutlet weak var studentNameLbl: UILabel!
    @IBOutlet weak var enrollmentLbl: UILabel!
    var subArr:Array<Any>!
    var checkbox = UIImage(named: "checkbox")
    var unCheckbox = UIImage(named: "uncheckbox")
    var studentRecord:StudentRecord!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVieww.layer.borderWidth = 1.0
        tableVieww.layer.borderColor = UIColor.darkGray.cgColor
        studentNameLbl.text = studentRecord.name
        enrollmentLbl.text = String(studentRecord.enrollmentNo)
        semesterLbl.text = String(studentRecord.semester)
        
       let scode =  DatabaseHandler.sharedInstance.getSubjectCode()
        if scode != nil {
            if studentRecord.semester == 1 {
                subArr = scode?.firstSemester.components(separatedBy: ",")
            }else  if studentRecord.semester ==  2
            {
                subArr = scode?.secondSemester.components(separatedBy: ",")
            }else  if studentRecord.semester ==  3
            {
                subArr = scode?.thirdSemester.components(separatedBy: ",")
            }else  if studentRecord.semester ==  4
            {
                subArr = scode?.fourthSemester.components(separatedBy: ",")
            }else  if studentRecord.semester ==  5
            {
                subArr = scode?.fifthSemester.components(separatedBy: ",")
            }
            tableVieww.reloadData()

        }
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stablecelll") as! TableAlertSCell
        cell.nameLbl?.text = String(describing: subArr[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark

    }

    
}


class TableAlertSCell: UITableViewCell {
    
    @IBOutlet var nameLbl: UILabel? = nil
    @IBOutlet weak var uncheckBtn: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


