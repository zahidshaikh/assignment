//
//  DatabaseHandler.swift
//  Assingment Submission and Result Management
//
//  Created by Zahid Shaikh on 22/09/17.
//  Copyright © 2017 Zahid Shaikh. All rights reserved.
//
import Foundation
import FMDB

class DatabaseHandler: NSObject {
    
    private static var __once: () = {
        Static.instance = DatabaseHandler()
    }()
    
    
    struct Static {
        static var onceToken: Int = 0
        static var instance: DatabaseHandler? = nil
    }
    
    class var sharedInstance: DatabaseHandler {
        
        _ = DatabaseHandler.__once
        return Static.instance!
    }
    
    var DBName = "ignou.db"
    
    func getDocumentPath(_ fileName: String) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(fileName)
        return fileURL.path
    }
    
    func copyFile(_ fileName: NSString) {
        
        let dbPath: String = getDocumentPath(fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            let documentsURL = Bundle.main.resourceURL
            let fromPath = documentsURL!.appendingPathComponent(fileName as String)
            var error : NSError?
            do {
                try fileManager.copyItem(atPath: fromPath.path, toPath: dbPath)
            } catch let error1 as NSError {
                error = error1
            }
        }
    }
    
    func createDatabase()-> Bool{
        
        let path = getDocumentPath(DBName)
        
        print(path)
        let exists:Bool = FileManager.default.fileExists(atPath: path)
        if (exists) {
            /* a file of the same name exists, we don't care about this so won't do anything */
            return true;
        }
        
        copyFile(DBName as NSString)
        
        return false
        
    }
    func saveStudentRecord(studentRecord: StudentRecord) {
//        CREATE TABLE Student (
//            Name             TEXT,
//            EnrollmentNumber INTEGER,
//            StudyCenterName  TEXT,
//            StudyCenterCode  INTEGER,
//            Session          INTEGER,
//            Year             INTEGER,
//            Semester         INTEGER
//        );
        let path = getDocumentPath(DBName)
        let database = FMDatabase(path: path)
        //        let database = FMDatabase(url: fileURL)
        
        guard database.open() else {
            print("Unable to open database")
            return
        }
        
        do {
            try database.executeUpdate("INSERT INTO Student (Name, EnrollmentNumber, StudyCenterName,StudyCenterCode,Session,Year,Semester) values (?, ?, ?, ?, ?, ?,?)", values: [studentRecord.name, studentRecord.enrollmentNo,studentRecord.studycenterName,studentRecord.studycenterCode,studentRecord.session,studentRecord.year,studentRecord.semester])
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        
        database.close()

        
    }
    
    func getRecordByEnrollmentNo(_ enrollmentNo:Int)->StudentRecord? {
        let path = getDocumentPath(DBName)
        let database = FMDatabase(path: path)
        var strec:StudentRecord?

        guard database.open() else {
            print("Unable to open database")
            return strec
        }
         
        do {
            let qwery = "SELECT Name, EnrollmentNumber, StudyCenterName,StudyCenterCode,Session,Year,Semester FROM Student WHERE EnrollmentNumber = \(enrollmentNo)"
            let rs = try database.executeQuery(qwery, values: nil)
            while rs.next() {
                strec = StudentRecord()
                strec?.name = rs.string(forColumn: "Name")!
                strec?.enrollmentNo = (rs.long(forColumn: "EnrollmentNumber"))
                strec?.studycenterCode = rs.string(forColumn: "StudyCenterCode")!
                strec?.session = rs.string(forColumn: "Session")!
                strec?.year = (rs.long(forColumn: "Year"))
                strec?.semester = (rs.long(forColumn: "Semester"))

            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        
        database.close()
        return strec
    }
    
    func getSubjectCode() -> SubjectCode? {
        let path = getDocumentPath(DBName)
        let database = FMDatabase(path: path)
        
        var sc:SubjectCode?
        guard database.open() else {
            print("Unable to open database")
            return sc
        }
        
        do {
            
            let rs = try database.executeQuery("SELECT FirstSemester,SecondSemester,ThirdSemester,FourthSemester,FifthSemester FROM SubjectCode", values: nil)
            while rs.next() {
             sc = SubjectCode()
                sc?.firstSemester = rs.string(forColumn: "FirstSemester")!
                sc?.secondSemester = rs.string(forColumn: "SecondSemester")!
                sc?.thirdSemester = rs.string(forColumn: "ThirdSemester")!
                sc?.fourthSemester = rs.string(forColumn: "FourthSemester")!
                sc?.fifthSemester = rs.string(forColumn: "FifthSemester")!
                
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
       database.close()
        return sc
    }
    /*
    //MARK:- Exists
    
    func isExists(_ rowid:String)-> Bool{
        
        let path = getDocumentPath(DBName)
        var isexists = false
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return isexists
        }
        
        do {
            
            let query = String(format: "SELECT %@", rowid)
            let rs = try database?.executeQuery(query, values: nil)
            while (rs?.next())! {
                isexists = true
                break
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        return isexists
    }
    
    //MARK:- Save Users
    
    func saveUser(_ user:ChatUser){
        
        
        let path = getDocumentPath(DBName)
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return
        }
        
        let query = String(format: "JID FROM ChatUser WHERE JID = '%@'", user.userJID)
        if isExists(query){
            
            do {
                
                try database?.executeUpdate("UPDATE ChatUser SET DateModified = ?, LastChatThread = ? WHERE JID = ?", values: [user.dateModified!, user.lastChatThread, user.userJID])
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            
            return
        }
        
        
        do {
            try database?.executeUpdate("INSERT INTO ChatUser (UserName, NickName, ProfilePicPath, JID, DateCreated ,IsActive,GroupJID,GroupUserID,IsGroup,GroupAdmin, LastChatThread,DateModified,ServerUserID, Available) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [ user.userName, user.nickName,user.profilePicPath,user.userJID,user.dateCreated,user.isActive,user.groupJID,user.groupUserID,user.isGroup,user.isGroupAdmin ,user.lastChatThread,user.dateCreated,user.serverUserID,user.isAvailable])
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        
    }
    
    func updateGroupUsers(_ user:ChatUser){
        
        
        let path = getDocumentPath(DBName)
        
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return
        }
        
        do {
            
            try database?.executeUpdate("UPDATE ChatUser SET GroupUserID = ? WHERE GroupJID = ?", values: [user.groupUserID, user.groupJID])
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        
    }
    
    
    func getAvailable(_ qurery:String)-> Bool{
        
        let path = getDocumentPath(DBName)
        var avai = false
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return avai
        }
        
        do {
            
            let sqlqurery = String(format: "SELECT Available FROM ChatUser WHERE %@", qurery)
            let rs = try database?.executeQuery(sqlqurery, values: nil)
            if (rs?.next())! {
                avai = (rs?.bool(forColumn: "Available"))!
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        
        return avai
        
    }
    
    func deleteChateUser(_ userid: Int) {
        
        let path = getDocumentPath(DBName)
        
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return
        }
        
        do {
            
            let query = String(format: "DELETE FROM ChatUser WHERE ID = ?")
            try database?.executeUpdate(query, values: [userid])
            
            
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
    }
    func getUserList(_ isbroup: Bool)-> NSMutableArray{
        
        let userArray = NSMutableArray()
        let path = getDocumentPath(DBName)
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return userArray
        }
        
        
        do {
            
            
            let sqlqurery = String(format: "SELECT ID, UserName, NickName, ProfilePicPath,JID, DateCreated, IsActive,GroupJID,GroupUserID,IsGroup,GroupAdmin,LastChatThread,DateModified,ServerUserID FROM ChatUser WHERE IsGroup = %@ ORDER BY DateModified DESC", isbroup as CVarArg)
            let rs = try database?.executeQuery(sqlqurery, values: nil)
            while (rs?.next())! {
                
                let cuser = ChatUser()
                cuser.userID = (rs?.long(forColumn: "ID"))!
                cuser.userName = (rs?.string(forColumn: "UserName"))!
                cuser.nickName = (rs?.string(forColumn: "NickName"))!
                cuser.profilePicPath = (rs?.string(forColumn: "ProfilePicPath"))!
                cuser.userJID = (rs?.string(forColumn: "JID"))!
                cuser.dateCreated = rs!.date(forColumn: "DateCreated")
                cuser.isActive = (rs?.long(forColumn: "IsActive"))!
                cuser.groupJID = (rs?.string(forColumn: "GroupJID"))!
                cuser.groupUserID = (rs?.string(forColumn: "GroupUserID"))!
                cuser.isGroup = (rs?.bool(forColumn: "IsGroup"))!
                cuser.isGroupAdmin = (rs?.bool(forColumn: "GroupAdmin"))!
                cuser.lastChatThread = (rs?.string(forColumn: "LastChatThread"))!
                cuser.dateModified = rs?.date(forColumn: "DateModified")
                cuser.serverUserID = (rs?.string(forColumn: "ServerUserID"))!
                
                cuser.lastChatThread = checkIncomingMessageType(cuser.lastChatThread)
                
                userArray.add(cuser)
                
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        
        return userArray
        
    }
    
    func getUserByGroupJID(_ isbroup: Bool, groupid:String)-> ChatUser{
        
        let path = getDocumentPath(DBName)
        let cuser = ChatUser()
        
        let database = FMDatabase(path: path)
        if !(database?.open())! {
            print("Unable to open database")
            return cuser
        }
        
        
        do {
            
            
            let sqlqurery = String(format: "SELECT ID, UserName, NickName, ProfilePicPath,JID, DateCreated, IsActive,GroupJID,GroupUserID,IsGroup,GroupAdmin,LastChatThread,DateModified,ServerUserID FROM ChatUser WHERE IsGroup = %@ AND GroupJID = '%@'", isbroup as CVarArg,groupid)
            let rs = try database?.executeQuery(sqlqurery, values: nil)
            if (rs?.next())! {
                
                cuser.userID = (rs?.long(forColumn: "ID"))!
                cuser.userName = (rs?.string(forColumn: "UserName"))!
                cuser.nickName = (rs?.string(forColumn: "NickName"))!
                cuser.profilePicPath = (rs?.string(forColumn: "ProfilePicPath"))!
                cuser.userJID = (rs?.string(forColumn: "JID"))!
                cuser.dateCreated = rs!.date(forColumn: "DateCreated")
                cuser.isActive = (rs?.long(forColumn: "IsActive"))!
                cuser.groupJID = (rs?.string(forColumn: "GroupJID"))!
                cuser.groupUserID = (rs?.string(forColumn: "GroupUserID"))!
                cuser.isGroup = (rs?.bool(forColumn: "IsGroup"))!
                cuser.isGroupAdmin = (rs?.bool(forColumn: "GroupAdmin"))!
                cuser.lastChatThread = (rs?.string(forColumn: "LastChatThread"))!
                cuser.dateModified = rs?.date(forColumn: "DateModified")
                cuser.serverUserID = (rs?.string(forColumn: "ServerUserID"))!
                
                cuser.lastChatThread = checkIncomingMessageType(cuser.lastChatThread)
                
                
            }
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        database?.close()
        
        return cuser
        
    }
   */
}

class StudentRecord {
    var name = ""
    var enrollmentNo = 0
    var studycenterName = ""
    var studycenterCode = ""
    var session = ""
    var year = 0
    var semester = 0
    
}

class SubjectCode {
    var firstSemester = ""
    var secondSemester = ""
    var thirdSemester = ""
    var fourthSemester = ""
    var fifthSemester = ""
}
